#! /usr/bin/env node

//var userArgs = process.argv.slice(2);
var fs = require("fs");
var swig = require('swig');
swig.setDefaults({
  varControls : ['<<', '>>'],
  tagControls : ['<%', '%>']
});
//var jsonData = require('data.json');
var mkdirp = require('mkdirp');

fs.readFile('data.json','utf8',function(err,data){
  if (err) throw err;
  var jsonData = JSON.parse(data);


  mkdirp('public/views/crud', function(err) {
    if(!err){
      createHtmlFiles(jsonData);
    }
  });
  mkdirp('public/services', function(err) {
    if(!err){
      mkdirp('public/routes', function(err) {
        if(!err){
          mkdirp('public/controllers', function(err) {
            if(!err){
              createPublicJSFiles(jsonData);
            }
          });
        }
      });
    }
  });
  mkdirp('server/models', function(err){
    if(!err){
      mkdirp('server/controllers', function(err){
        if(!err){
          mkdirp('server/routes', function(err){
            if(!err){
              createServerJSFiles(jsonData);
            }
          });
        }
      });
    }
  });
})


function createServerJSFiles(jsonData) {
  var tmplForControllerJS = swig.compileFile(__dirname + '/templates/serverjs/controllers.js');
  var output = tmplForControllerJS(jsonData);
  fs.writeFile('server/controllers/'+jsonData.modelPlural+'.js', output);

  var tmplForModelJS = swig.compileFile(__dirname + '/templates/serverjs/models.js');
  output = tmplForModelJS(jsonData);
  fs.writeFile('server/models/'+jsonData.modelName+'.js', output);

  var tmplForRoutesJS = swig.compileFile(__dirname + '/templates/serverjs/routes.js');
  output = tmplForRoutesJS(jsonData);
  fs.writeFile('server/routes/'+jsonData.modelPlural+'.js', output);
}

function createPublicJSFiles(jsonData) {
  var tmplForServicesJS = swig.compileFile(__dirname + '/templates/publicjs/services.js');
  var output = tmplForServicesJS(jsonData);
  fs.writeFile('public/services/'+jsonData.modelPlural+'.js', output);

  var tmplForRoutesJS = swig.compileFile(__dirname + '/templates/publicjs/routes.js');
  output = tmplForRoutesJS(jsonData);
  fs.writeFile('public/routes/'+jsonData.modelPlural+'.js', output);

  var tmplForControllerJS = swig.compileFile(__dirname + '/templates/publicjs/controllers.js');
  output = tmplForControllerJS(jsonData);
  fs.writeFile('public/controllers/'+jsonData.modelPlural+'.js', output);

}

function createHtmlFiles(jsonData){
  var tmplForViewhtml = swig.compileFile(__dirname + '/templates/crud/view.html');
  var output = tmplForViewhtml(jsonData);
  fs.writeFile('public/views/crud/view.html', output);

  var tmplForEditHtml = swig.compileFile(__dirname + '/templates/crud/edit.html');
  output = tmplForEditHtml(jsonData);
  fs.writeFile('public/views/crud/edit.html', output);

  var tmplForCreateHtml = swig.compileFile(__dirname + '/templates/crud/create.html');
  output = tmplForCreateHtml(jsonData);
  fs.writeFile('public/views/crud/create.html', output);

  var tmplForListHtml = swig.compileFile(__dirname + '/templates/crud/list.html');
  output = tmplForListHtml(jsonData);
  fs.writeFile('public/views/crud/list.html', output);

  var tmplForFormHtml = swig.compileFile(__dirname + '/templates/crud/_form.html');
  output = tmplForFormHtml(jsonData);
  fs.writeFile('public/views/crud/_form.html', output);
}
