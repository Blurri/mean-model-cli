'use strict';


// The Package is past automatically as first parameter
module.exports = function(<<modelPlural || title>>, app, auth, database) {
  var <<modelPlural>> = require('../controllers/<<modelPlural>>')(<<modelPlural || title>>);

  app.route('/api/<<modelPlural>>')
    .get(<<modelPlural>>.all)
    .post(auth.requiresLogin, <<modelPlural>>.create);
  app.route('/api/<<modelPlural>>/:<<modelName>>Id')
    .get(auth.isMongoId, <<modelPlural>>.show)
    .put(auth.isMongoId, auth.requiresLogin, <<modelPlural>>.update)
    .delete(auth.isMongoId, auth.requiresLogin, <<modelPlural>>.destroy);

  // Finish with setting up the <<modelName>>Id param
  app.param('<<modelName>>Id', <<modelPlural>>.<<modelName>>);
};
