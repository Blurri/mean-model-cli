'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * <<modelName | title>> Schema
 */
var <<modelName | title>>Schema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  <% for property in properties -%>
    <%- if property.type ===  'string' -%>
      <<property.name>> : {
        type : String,
        trim : true
      }
    <%- endif -%>
    <%- if property.type ===  'number' -%>
      <<property.name>> : {
        type : Number
      }
    <%- endif -%>
    <%- if property.type ===  'date' -%>
      <<property.name>> : {
        type : Date
      }
    <%- endif -%>
    <%- if not loop.last -%>,<%- endif -%>
  <%- endfor %>
});


/**
 * Statics
 */
<<modelName | title>>Schema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).exec(cb);
};

mongoose.model('<<modelName | title>>', <<modelName | title>>Schema);
