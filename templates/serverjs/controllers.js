'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    <<modelName | title>> = mongoose.model('<<modelName | title>>'),
    _ = require('lodash');

module.exports = function(<<modelPlural | title>>) {

      return {
          /**
           * Find <<modelName>> by id
           */
          <<modelName>>: function(req, res, next, id) {
              <<modelName | title>>.load(id, function(err, <<modelName>>) {
                  if (err) return next(err);
                  if (!<<modelName>>) return next(new Error('Failed to load <<modelName>> ' + id));
                  req.<<modelName>> = <<modelName>>;
                  next();
              });
          },
          /**
           * Create an <<modelName>>
           */
          create: function(req, res) {
              var <<modelName>> = new <<modelName | title>>(req.body);
              <<modelName>>.save(function(err) {
                  if (err) {
                      return res.status(500).json({
                          error: 'Cannot save the <<modelName>>'
                      });
                  }
                  res.json(<<modelName>>);
              });
          },
          /**
           * Update an <<modelName>>
           */
          update: function(req, res) {
              var <<modelName>> = req.<<modelName>>;
              <<modelName>> = _.extend(<<modelName>>, req.body);
              <<modelName>>.save(function(err) {
                  if (err) {
                      return res.status(500).json({
                          error: 'Cannot update the <<modelName>>'
                      });
                  }
                  res.json(<<modelName>>);
              });
          },
          /**
           * Delete an <<modelName>>
           */
          destroy: function(req, res) {
              var <<modelName>> = req.<<modelName>>;
              <<modelName>>.remove(function(err) {
                  if (err) {
                      return res.status(500).json({
                          error: 'Cannot delete the <<modelName>>'
                      });
                  }
                  res.json(<<modelName>>);
              });
          },
          /**
           * Show an <<modelName>>
           */
          show: function(req, res) {
              res.json(req.<<modelName>>);
          },
          /**
           * List of <<modelPlural | title>>
           */
          all: function(req, res) {
              <<modelName | title>>.find({}).sort('-created').exec(function(err, <<modelPlural>>) {
                  if (err) {
                      return res.status(500).json({
                          error: 'Cannot list the <<modelPlural>>'
                      });
                  }
                  res.json(<<modelPlural>>)
              });
          }
      };
  }
