'use strict';

angular.module('mean.<<moduleName>>').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider
      .state('all <<modelName>>', {
        url: '/<<modelPlural>>',
        templateUrl: '/<<modelPlural>>/views/crud/list.html',
        resolve : {
          loggedin : function(MeanUser){
            return MeanUser.checkLoggedin();
          }
        }
      })
      .state('create <<modelName>>', {
        url: '/<<modelPlural>>/create',
        templateUrl: '/<<modelPlural>>/views/crud/create.html',
        resolve : {
          loggedin : function(MeanUser){
            return MeanUser.checkLoggedin();
          }
        }
      })
      .state('edit <<modelName>>', {
        url: '/<<modelPlural>>/:<<modelName>>Id/edit',
        templateUrl: '/<<modelPlural>>/views/crud/edit.html',
        resolve : {
          loggedin : function(MeanUser){
            return MeanUser.checkLoggedin();
          }
        }
      })
      .state('<<modelName>> by id', {
        url: '/<<modelPlural>>/:<<modelName>>Id',
        templateUrl: '/<<modelPlural>>/views/crud/view.html',
        resolve : {
          loggedin : function(MeanUser){
            return MeanUser.checkLoggedin();
          }
        }
      })

  }
]);
