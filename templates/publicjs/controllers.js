'use strict';

angular.module('mean.<<moduleName>>').controller('<<modelPlural | title>>Controller', ['$scope', 'Global', '<<modelPlural | title>>','$location','$stateParams',
  function($scope, Global, <<modelPlural | title>>, $location, $stateParams) {
    $scope.global = Global;
    $scope.package = {
      name: '<<modelPlural>>'
    };
    $scope.<<modelName>> = {};

    $scope.create = function() {
        var <<modelName>> = new <<modelPlural | title>>($scope.<<modelName>>);
        <<modelName>>.$save(function(response){
          $location.path('<<modelPlural>>/'+response._id);
        });
        $scope.<<modelName>> = {};
    };

    $scope.update = function() {
        var <<modelName>> = $scope.<<modelName>>;
        <<modelName>>.$update(function() {
          $location.path('<<modelPlural>>/' + <<modelName>>._id);
        });
    };

    $scope.findOne = function() {
      <<modelPlural | title >>.get({
        <<modelName>>Id: $stateParams.<<modelName>>Id
      }, function(<<modelName>>) {
        <% for property in properties -%>
          <%- if property.type === 'date' -%>
            << modelName >>.<<property.name>> = new Date(<< modelName >>.<<property.name>>);
          <%- endif -%>
        <%- endfor -%>
        $scope.<<modelName>> = <<modelName>>;
      });
    };

    $scope.find = function() {
      <<modelPlural | title>>.query(function(<<modelPlural>>) {
        $scope.<<modelPlural>> = <<modelPlural>>;
      });
    };
  }

]);
