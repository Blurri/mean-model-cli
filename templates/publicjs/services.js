'use strict';

angular.module('mean.<<moduleName>>').factory('<<modelPlural | title>>', ['$resource',
  function($resource) {
    return $resource('api/<<modelPlural>>/:<<modelName>>Id', {
      customerId : '@_id'
    }, {
      update : {
        method : 'PUT'
      }
    });
  }
]);
